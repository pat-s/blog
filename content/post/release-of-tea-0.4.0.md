---
date: "2020-07-18T20:00:00+00:00"
author: "6543"
title: "tea 0.4.0 is released"
tags: ["release", "tea"]
draft: false
---

We are proud to present you `tea`, a CLI tool that allows you to work with pull requests, issues and more in your terminal.

With the new v0.4.0 release tea can now:
 * list issues, pulls, releases, repos, notifications and times
 * create issues, pulls and releases
 * add and deltete times
 * manage labels
 * more comming ...

<!--more-->

![gif](https://dl.gitea.io/screenshots/tea_demo.gif)

## Changelog

## [v0.4.0](https://gitea.com/gitea/tea/pulls?q=&type=all&state=closed&milestone=1264) - 2020-07-18

* FEATURES
  * Add notifications subcomand ([#148](https://github.com/go-gitea/gitea/pull/148))
  * Add subcomand 'pulls create' ([#144](https://github.com/go-gitea/gitea/pull/144))
* BUGFIXES
  * Fix Login Detection By Repo Param ([#151](https://github.com/go-gitea/gitea/pull/151))
  * Fix Login List Output ([#150](https://github.com/go-gitea/gitea/pull/150))
  * Fix --ssh-key Option ([#135](https://github.com/go-gitea/gitea/pull/135))
* ENHANCEMENTS
  * Subcomand Login Show List By Default ([#152](https://github.com/go-gitea/gitea/pull/152))
* BUILD
  * Migrate src-d/go-git to go-git/go-git ([#128](https://github.com/go-gitea/gitea/pull/128))
  * Migrate gitea-sdk to v0.12.0 ([#133](https://github.com/go-gitea/gitea/pull/133))
  * Migrate yaml lib ([#130](https://github.com/go-gitea/gitea/pull/130))
  * Add gitea-vet ([#121](https://github.com/go-gitea/gitea/pull/121))

[More](https://gitea.com/gitea/tea/src/branch/master/CHANGELOG.md)
